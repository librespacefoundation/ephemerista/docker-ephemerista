FROM ghcr.io/astral-sh/uv:python3.12-bookworm

RUN apt-get -qq update
RUN apt-get install -y curl maven jq \
  && curl -sL https://deb.nodesource.com/setup_20.x | bash - \
  && apt-get install -y nodejs
RUN uv tool install rust-just

ENV PATH="/root/.local/bin:$PATH"
